function createNewUser() {
    let name1 = prompt('Please, enter your first name');
    let name2 = prompt('Please, enter your second name');
    let bDay = prompt('please enter your date birthday yyyy. mm. dd');
    let userBDay = new Date(bDay);


    let newUser = {
        firstName: name1,
        lastName: name2,
        birthday: userBDay,

        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase()
        },
        getAge: function () {
            let today = new Date();
            let age = today - this.birthday;
            return  Math.floor (age / (1000 * 3600 * 365 * 24));
        },
        getPassword: function () {
            return ((this.firstName.charAt(0).toLocaleUpperCase() + this.lastName.toLowerCase())+ this.birthday.getFullYear());
        }
    };
    return newUser;
}


let user = createNewUser();
// let secondUser = createNewUser;
// let thirdUser = secondUser();
// let fourUser = secondUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

// console.log(thirdUser);
// console.log(fourUser);
